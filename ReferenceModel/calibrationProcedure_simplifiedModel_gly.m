%% Code for calibration of model with coupled carbon and energy balances: growth of BW on glycerol
% "Resource allocation accounts for the large variability of rate-yield phenotypes across bacterial strains" 
% calibrationProcedure_simplifiedModel_gly.m
% Authors: Valentina Baldazzi and Hidde de Jong, March 2022

% rates
mu = 0.49; %h-1, Haverkorn et al.
g = 0.027 * 1.2; %h-1, Esquerre et al. and Farmer et al. (1.2)

% density
beta = 0.0246; %gDW Cmmol-1, Esquerre et al.

% metabolite concentrations
totalFreeMetabConc = 1.2; % Cmmol gDW-1, Gerosa et al. and Bennett et al.
metabFrac = totalFreeMetabConc / (1/beta);

fracC = 0.17; % Gerosa et al.

c = fracC * totalFreeMetabConc; % Cmmol gDW-1

% protein fractions and concentrations
protFrac = (0.73 * 1.0) + (0.5 * metabFrac); % Basan et al., for corresponding growth rate of NCM strain

fracM_u = 0.36; % Schmidt et al., housekeeping (residual) proteins
fracR = 0.225 + 0.155; % Schmidt et al., ribosomes and enzyme amino acid metabolism
fracM_c = 0.10; % Schmidt et al., central carbon metabolism and uptake 
fracM_e = 0.16; % Schmidt et al., energy metabolism

m_u = fracM_u * protFrac * (1/beta); % Cmmol gDW-1
r = fracR * protFrac * (1/beta); % Cmmol gDW-1
m_c = fracM_c * protFrac * (1/beta); % Cmmol gDW-1
m_e = fracM_e * protFrac * (1/beta); % Cmmol gDW-1

totalProt = m_u + r + m_c + m_e; % Cmmol gDW-1

% other macromolecules
u = 1/beta - c - m_u - r - m_c - m_e; % Cmmol gDW-1

nonProt = c + u; % Cmmol gDW-1

as = 0.005; %mmol_ATP gDW-1 h-1, Gerosa et al. / include NADH NADPH
a = 0.01; %mmol_ATP gDW-1 h-1, Gerosa et al. / include NAD NADP

a0 = as + a; %mmol_ATP gDW-1 h-1

% measured uptake and fermentation rates
v_gly = 11.3; %mmol_glc gDW-1 h-1, Haverkorn et al.
v_ace = 0.60; %mmol_ace gDW-1 h-1, Haverkorn et al.

v_mc = 3 * v_gly;
v_mef = 2 * v_ace;

% inferred biosynthesis rates
v_r = (mu + g) * totalProt; %Cmmol gDW-1 h-1
v_mu = (mu + g) * u; %Cmmol gDW-1 h-1

% CO2 rate correction factors for biosynthesis and fermentation rates
corr_mef = 1.5; % 1 CO2 / acetate, Basan et al.

eta = 7.2; % Cmmol gDW-1 , growth-rate-dependent CO2 loss for biomass synthesis, Fischer et al. and Basan et al.
v_rq_CO2 = eta * mu; % Cmmol gDW-1 h-1
corr_ru = (v_r + v_mu + v_rq_CO2) / (v_r + v_mu);

% inferred respiration rate
v_CO2_tot = v_mc - v_mef - (mu + g) * (1/beta); %Cmmol gDW-1 h-1
v_mef_CO2 = (corr_mef - 1) * v_mef;
v_mer = v_CO2_tot - v_mef_CO2 - v_rq_CO2;

% energy yield coefficients
n_mer = 14/3; % (mmol_ATP/mmol_gly) / (Cmmol_gly/mmol_gly) = mmol_ATP/Cmmol_gly
n_mef =  7/3; % (mmol_ATP/mmol_gly) / (Cmmol_gly/mmol_gly) = mmol_ATP/Cmmol_gly
n_r = 0.44/4.8; % (mmol_ATP/mmol_aa) / (Cmmol_aa/mmol_aa) = mmol_ATP/Cmmol_aa, Kaleta et al.
n_mu = 0.65; % Russel & Cook, glucose

v_spill= (n_mer * v_mer + n_mef * v_mef)-(n_r * v_r + n_mu * v_mu);
k_a= v_spill/as;

% kinetic parameters uptake flux
e_s = v_mc / m_c;

sat_coeff = 1.2; % substrate saturation, Dourado et al. and Gerosa et al., conversion 3.43

% kinetic parameters energy fluxes (respiration and fermentation
K_amer = a/10; %mmol_ATP gDW-1
K_mer = c/sat_coeff; %Cmmol gDW-1
K_amef = a/10; %mmol_ATP gDW-1
K_mef = c/sat_coeff; %Cmmol gDW-1

mer_fraction = 0.37 + (0.30 + 0.32)*(v_mer/(v_mer + v_mef)); % computed from data Schmidt et al.
mef_fraction = 0.01 + (0.30 + 0.32)*(v_mef/(v_mer + v_mef)); % computed from data Schmidt et al.

m_er = m_e * mer_fraction; %Cmmol gDW-1
m_ef = m_e * mef_fraction; %Cmmol gDW-1

k_mer = v_mer /((a/(a+K_amer)) * (c/(c+K_mer)) * m_er); %h-1
k_mef = v_mef /((a/(a+K_amef)) * (c/(c+K_mef)) * m_ef); %h-1

% kinetic parameters other biomass flux
K_mu = c/sat_coeff; %Cmmol gDW-1
K_amu = as/10; %Cmmol gDW-1

k_mu = v_mu /((as/(as+K_amu)) * (c/(c+K_mu)) * m_u); %h-1

% kinetic parameters translation flux
K_r = c/sat_coeff; % Cmmol gDW-1
K_ar = as/10; % Cmmol gDW-1

k_r = v_r /((as/(as+K_ar)) * (c/(c+K_r)) * r); %h-1

% compute resource allocation parameters
chi_u = fracM_u;
chi_r = fracR;
chi_c = fracM_c;
chi_er = fracM_e * mer_fraction;

% collect fluxes, rates, concentrations, and resource allocation parameters in table
tableFRC= table(mu, g, v_mer, v_mef, v_mu, v_mc, v_r, beta, m_u, r, m_c, m_e, m_er, m_ef, v_CO2_tot, c, u, a, as, a0, chi_u, chi_r, chi_c, chi_er);

disp(tableFRC);
writetable(tableFRC,'tableFRC_simplified_gly.dat');

% collect kinetic parameters in table
tableP=table(n_mer, n_mef, n_r, n_mu, e_s, K_amer, K_amef, K_mer, K_mef, k_mer, k_mef, K_amu, K_mu, k_mu, K_ar, K_r, k_r, corr_mef, corr_ru, k_a);

disp(tableP);
writetable(tableP,'tableP_simplified_gly.dat');

