%% Code for the sampling of the space of resource allocation parameters
% "Resource allocation accounts for the large variability of rate-yield phenotypes across bacterial strains" 
% Sampling_generic_paper.m
% Authors: Valentina Baldazzi and Hidde de Jong, March 2022

clear all;
close all;

%% Define simulation scenario (carbon source)
global scenario

scenario = 'glc_MG'; % glc, gly, glc02, glc035, glc05, glc_varying_parameters, glc_MG

%% Define and read parameters
global beta gamma n_r n_mu e_s n_mer n_mef; 
global k_mu k_r k_mer k_mef k_a;
global K_mer K_mef K_mu K_r K_ar K_amu K_amer K_amef;
global a0;
global corr_ru corr_mef;


    
if strcmp(scenario, 'glc')||strcmp(scenario, 'glc_varying_parameters')
    parameters = readtable('tableP_simplified_glc.dat');
    ratesConc = readtable('tableFRC_simplified_glc.dat');
elseif strcmp(scenario, 'gly')
    parameters = readtable('tableP_simplified_gly.dat');
    ratesConc = readtable('tableFRC_simplified_gly.dat');
elseif strcmp(scenario, 'glc02')
    parameters = readtable('./tableP_simplified_glc_mu02.dat');
    ratesConc = readtable('./tableFRC_simplified_glc_mu02.dat');    
elseif strcmp(scenario, 'glc035')
    parameters = readtable('tableP_simplified_glc_mu035.dat');
    ratesConc = readtable('tableFRC_simplified_glc_mu035.dat');
elseif strcmp(scenario, 'glc05')
    parameters = readtable('./tableP_simplified_glc_mu05.dat');
    ratesConc = readtable('./tableFRC_simplified_glc_mu05.dat');   
elseif strcmp(scenario, 'glc_NCM')
    parameters = readtable('tableP_simplified_glc_NCM.dat');
    ratesConc = readtable('tableFRC_simplified_glc_NCM.dat');
elseif strcmp(scenario, 'glc_MG')
    parameters = readtable('tableP_simplified_glc_MG.dat');
    ratesConc = readtable('tableFRC_simplified_glc_MG.dat');  
end

% Define parameters
beta = ratesConc.beta;
gamma = ratesConc.g;

n_r = parameters.n_r; 
n_mu = parameters.n_mu; 

e_s = parameters.e_s;

k_mu = parameters.k_mu;
k_r = parameters.k_r;
k_a = parameters.k_a;

K_mu = parameters.K_mu;
K_r = parameters.K_r;
K_ar = parameters.K_ar;
K_amu =  parameters.K_amu;

a0 = ratesConc.a0;

corr_ru = parameters.corr_ru;
corr_mef = parameters.corr_mef;

K_amer =  parameters.K_amer;
K_mer = parameters.K_mer;
n_mer = parameters.n_mer; 
k_mer = parameters.k_mer;
k_mef = parameters.k_mef;
K_amef =  parameters.K_amef;
K_mef = parameters.K_mef;
n_mef = parameters.n_mef; 

%% Simulation to steady-state growth

% Simulation parameters
showSim = 0;
tstart = 0;
tfinal = 400;
tspan = [tstart:1:tfinal];
options = odeset('InitialStep', 1e-6, 'RelTol', 1e-6,'AbsTol', 1e-8);

nsamples = 50; %number of samples (random) or grid size (nsample x nsample)
samplingMethod = [1, 0, 0]; % [fermentation (0/1), constant m_u (0/1), sampling on grid (0/1)]

samples = generateSamples(nsamples, samplingMethod, ratesConc.chi_u);


%% Initial conditions
if strcmp(scenario, 'gly')
    steady_variables = zeros(nsamples,8+4);
    x0 = [12, 5, 1, 3, 12, 0.15, 0.01];  % r, m_er,_m_ef, m_c, m_u, c, a* in Cmmol/gDW 
elseif strcmp(scenario, 'gal')
    steady_variables = zeros(nsamples,8+4);
    x0 = [10, 8, 0, 4.5, 12, 0.15, 0.01];  % r, m_e,_, m_c, m_u, c, a* in Cmmol/gDW
elseif strcmp(scenario, 'glc')||strcmp(scenario, 'glc_NCM')||strcmp(scenario, 'glc_MG')
    steady_variables = zeros(nsamples,8+4);
    x0 = [13, 2, 1, 3, 11, 0.2, 0.007];  % r, m_er, m_ef, m_c, m_u, c, a* in Cmmol/gDW
elseif strcmp(scenario, 'glc02')
    steady_variables = zeros(nsamples,8+4);
    x0 = [13, 2, 1, 3, 11, 0.2, 0.007];  % r, m_er, m_ef, m_c, m_u, c, a* in Cmmol/gDW
elseif  strcmp(scenario, 'glc035')
    steady_variables = zeros(nsamples,8+4);
    x0 = [8 ,6 ,1 ,3 ,10, 0.2, 0.007];% r, m_er, m_ef, m_c, m_u, c, a* in Cmmol/gDW
elseif  strcmp(scenario, 'glc05')
    steady_variables = zeros(nsamples,8+4);
    x0 = [10 ,5 ,1 ,3 ,10, 0.2, 0.007];% r, m_er, m_ef, m_c, m_u, c, a* in Cmmol/gDW
elseif strcmp(scenario, 'glc_varying_parameters')
    steady_variables = zeros(nsamples,8+4);
    steady_flux = zeros(nsamples,5);
    param_samples = cell(nsamples,3);
    x0 = [13, 2, 1, 3, 11, 0.2, 0.007];  % r, m_er, m_ef, m_c, m_u, c, a* in Cmmol/gDW
end


 param0 = [e_s k_mu k_r k_mer k_mef K_mu K_r K_amu K_ar K_mer K_mef K_amer K_amef];
 param_names = ["e_s", "k_mu", "k_r", "k_mer", "k_mef", "K_mu", "K_r", "K_amu","K_ar", "K_mer", "K_mef", "K_amer", "K_amef"];
 param = param0;

for i=1:length(samples(:,1))
  
    if strcmp(scenario, 'glc_varying_parameters')
      % choice of parameter to be modified
       np = [1,4,5]; %e_s; k_mer, k_mef
       
    
       %random modification of the parameter(s) by a factor 2
       param = param0;
       p=param(np);
       for j = 1:length(np)
           p(j) = param0(np(j))*(0.5 + 1.5*rand); %independent modification of np params
             
       end    
    
       param(np) = p;
       param_samples(i,:) = {np,param_names(np),p};

    end

    % Show progress
    if mod(i,200)==0 
        disp(i); 
    end  
 
if ~(samples(i,:)==0 ) %for glc_grid case
     [tout,yout] = ode23s(@(t,y) coli_energy_ode_generic(t,y, samples(i,:), param),[tstart tfinal],x0, options);


   % Store values of variables at steady state
   steady_variables(i,1) = yout(end,1); %r
   steady_variables(i,2) = yout(end,2); %m_er
   steady_variables(i,3) = yout(end,3); %m_ef
   steady_variables(i,4) = yout(end,4); %m_c
   steady_variables(i,5) = yout(end,5); %m_u
   steady_variables(i,6) = yout(end,6); %c
   steady_variables(i,7) = yout(end,7); %a*
   steady_variables(i,8) = 1/beta - yout(end,1) - yout(end,2) - yout(end,3) - yout(end,4) - yout(end,5) - yout(end,6); %u = 1/beta - r - m_er - m_ef - m_c - m_u - c
   steady_variables(i,9) = samples(i,1); %chi_u
   steady_variables(i,10) = samples(i,2); %chi_r
   steady_variables(i,11) = samples(i,3); %chi_c; 
   steady_variables(i,12) = samples(i,4); %chi_er; 


   if showSim % Show simulation results for iteration
        figure;
        plot(tout,yout);
        disp([samples(i,1), samples(i,2), samples(i,3), samples(i,4)]); 
        waitforbuttonpress;
        close(gcf);
   end

else
    steady_variables(i,:)=0;
end

   if  strcmp(scenario, 'glc_varying_parameters')
        steady_flux(i,:) =  calculFlux_random_param(steady_variables(i,1:8)',param,1);
   end

end

% Compute values of fluxes at steady states
if  ~strcmp(scenario, 'glc_varying_parameters')
    steady_flux = calculFlux_generic_simplifiedModel(steady_variables(:,1:8)',size(steady_variables,1));
end


%% Save data
if  strcmp(scenario, 'glc_varying_parameters')
    save('Sampling_test2.mat', 'steady_variables','steady_flux', 'param_samples');
  
else
      save('Sampling_test.mat', 'steady_variables','steady_flux');
end

%% Compute fluxes, concentrations, and resource allocation parameters

v_mc = steady_flux(:,1);
v_mer = steady_flux(:,2);
v_mef = steady_flux(:,3);
v_r = steady_flux(:,4);
v_mu = steady_flux(:,5);

r = steady_variables(:,1);
m_er = steady_variables(:,2);
m_ef = steady_variables(:,3);
m_c = steady_variables(:,4);
m_u = steady_variables(:,5);
c = steady_variables(:,6);
as = steady_variables(:,7);
u = steady_variables(:,8);

chi_u = steady_variables(:,9);
chi_r = steady_variables(:,10);
chi_c = steady_variables(:,11);  
chi_er = steady_variables(:,12);

chi_e = 1-chi_u -chi_r-chi_c;

% compute rate and yield
mu = beta * (v_mc - v_mer - corr_mef*v_mef - (corr_ru-1)*v_mu - (corr_ru-1)*v_r) - gamma;  %mu
Y = (v_mc - v_mer - corr_mef*v_mef - (corr_ru-1)*v_mu - (corr_ru-1)*v_r - gamma/beta) ./ v_mc; %Y


%% Plot rate-yield plane

figure; hold on;
plot(mu,Y,'LineStyle','none','Marker','o','MarkerSize',3,'Color',[0.8,0.8,0.8] ); %
xlim([0,1.3]);
ylim([0,1]);
xlabel('Growth rate (h^{-1})');
ylabel('Growth yield');



%% Auxiliary functions

function s = generateSamples(ns, sm, p_u)
    % ns = number of samples
    % sm = sampling method
    % sc = scenario
    % p_u = chi_u value from calibration
    % s = matrix of samples (dimensions: nsx4)

    fermentation = sm(1);
    muConstant = sm(2);
    gridSampling = sm(3);

    if gridSampling

        s = zeros((ns-1)^2,4); % initialize resource allocation samples

        i=1; % counter
        for r = 1/ns:1/ns:(ns-1)/ns
            for c = 1/ns:1/ns:(ns-1)/ns
                chi_u = p_u;
                chi_er = 0.062283; %BW calibration value
                if (1 - chi_u - chi_er - r - c) > 0
                    chi_r = r;
                    chi_c = c;   
                    s(i,:) = [chi_u, chi_r, chi_c, chi_er]; %allocation scheme
                end
               i = i+1;
            end
        end
        
        if not(muConstant)
            disp('variable M_u fraction not supported for sampling on a grid ');
            return;
        end
    else %no sampling on grid


          rng shuffle

        % Set resource allocation schemes
        for i = 1:ns
            if muConstant
                chi_u = p_u;
            else
              
             chi_u=  0.32 + 0.09*rand; %schmidt data
              
            end


            chi_r = (1 - chi_u) * rand;
            chi_c = (1 - chi_u - chi_r) * rand;

            if fermentation
                chi_er = (1 - chi_u - chi_r - chi_c) * rand;
            else
                chi_er = (1 - chi_u - chi_r - chi_c);      
            end
            s(i,:) = [chi_u, chi_r, chi_c, chi_er]; % save allocation scheme
        end   
    end
end

