function v = calculFlux_random_param(x,p,n)

     global a0;

     v = zeros(n,5);
%% set parameter values

     e_s = p(1);
     k_mu = p(2);
     k_r = p(3);
     k_mer = p(4);
     k_mef = p(5);
     K_mu = p(6);
     K_r = p(7);
     K_amu = p(8);
     K_ar = p(9);
     K_mer = p(10);
     K_mef = p(11);
     K_amer = p(12);
     K_amef = p(13);

 
    %% calcul

     r = x(1,:);
     m_er = x(2,:);
     m_ef = x(3,:);
     m_c = x(4,:);
     m_u = x(5,:);
     c = x(6,:);
     as = x(7,:);
    
     v(:,1) = m_c * e_s; %v_mc
     v(:,2) = m_er .* k_mer .* (c ./ (c + K_mer)) .* ((a0 - as) ./ (a0 - as + K_amer)); %v_mer
     v(:,3) = m_ef .* k_mef .* (c ./ (c + K_mef)) .* ((a0 - as) ./ (a0 - as + K_amef)); %v_mef
     v(:,4) = k_r * r .* (c ./ (K_r + c)) .* (as ./ (as + K_ar));%v_r
     v(:,5) = k_mu * m_u .* (c ./ (K_mu + c)) .* (as ./ (as + K_amu));%v_mu


end
