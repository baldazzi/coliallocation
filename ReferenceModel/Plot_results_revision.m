%% Code for the generation of the figures
% "Resource allocation accounts for the large variability of rate-yield phenotypes across bacterial strains" 
% Plot_results_paper.m
% Authors: Valentina Baldazzi and Hidde de Jong, March 2022

clear all;
close all;

%% Define simulation scenario (carbon source)
global scenario

scenario = 'glc'; % glc, glc_varying_parameters, gly, chemostat, glc_3D, glc_grid, glc_MG, glc_NCM

%% Define and read parameters
global beta gamma n_r n_mu e_s n_mer n_mef; 
global k_mu k_r k_mer k_mef k_a;
global K_mer K_mef K_mu K_r K_ar K_amu K_amer K_amef;
global a0;
global corr_ru corr_mef;

if strcmp(scenario, 'glc') || strcmp(scenario, 'glc_varying_parameters')  || strcmp(scenario, 'glc_3D') || strcmp(scenario, 'glc_grid') ||strcmp(scenario, 'glc_NCM')
    parameters = readtable('tableP_simplified_glc.dat');
    ratesConc = readtable('tableFRC_simplified_glc.dat');
elseif strcmp(scenario, 'gly')
    parameters = readtable('tableP_simplified_gly.dat');
    ratesConc = readtable('tableFRC_simplified_gly.dat');
elseif strcmp(scenario, 'chemostat')
    parameters = readtable('./tableP_simplified_glc_mu02.dat');
    ratesConc = readtable('./tableFRC_simplified_glc_mu02.dat');
elseif strcmp(scenario, 'glc_MG')
     parameters = readtable('tableP_simplified_glc_MG.dat');
    ratesConc = readtable('tableFRC_simplified_glc_MG.dat');
end


% Retrieve parameter values
beta = ratesConc.beta;
gamma = ratesConc.g;

n_r = parameters.n_r; 
n_mu = parameters.n_mu; 

n_mer = parameters.n_mer; 
n_mef = parameters.n_mef; 

corr_mef = parameters.corr_mef;
corr_ru = parameters.corr_ru;

k_a= parameters.k_a;

K_ar = parameters.K_ar;
K_r = parameters.K_r;
K_amer= parameters.K_amer;

%% Load data and plot data
if strcmp(scenario, 'glc') || strcmp(scenario, 'glc_NCM')
   %load('Sampling_Glc_Km12.mat')
   load('Sampling_Glc_Km12_variableMu.mat')
elseif strcmp(scenario, 'gly')
    load('Sampling_Gly_VariableMu.mat');
elseif strcmp(scenario,'chemostat')
  load('Sampling_GlcD02_variableMu.mat');
elseif strcmp(scenario, 'glc_varying_parameters')
    load('Sampling_Glc_variableMu_variablek.mat');
elseif strcmp(scenario, 'glc_3D')
    load('SamplingGlc_extra3D.mat');
elseif strcmp(scenario, 'glc_grid') 
     load('Sampling_Glc_grid_chie006.mat'); % (BW WT value)
elseif strcmp(scenario, 'glc_MG')
    load('Sampling_Glc_MG.mat');

end  


%% Compute fluxes, concentrations, rates, and yield
% Compute fluxes
v_mc = steady_flux(:,1);
v_mer = steady_flux(:,2);
v_mef = steady_flux(:,3);
v_r = steady_flux(:,4);
v_mu = steady_flux(:,5);

% Compute concentrations
r = steady_variables(:,1);
m_er = steady_variables(:,2);
m_ef = steady_variables(:,3);
m_c = steady_variables(:,4);
m_u = steady_variables(:,5);
c = steady_variables(:,6);
as = steady_variables(:,7);
u = steady_variables(:,8);

p = r+m_ef+m_er+m_c+m_u;

chi_u = steady_variables(:,9);
chi_r = steady_variables(:,10);
chi_c = steady_variables(:,11);  
chi_er = steady_variables(:,12);

chi_e = 1-chi_u -chi_r-chi_c;

% Compute rate and yield
mu = beta * (v_mc - v_mer - corr_mef*v_mef - (corr_ru-1)*v_mu - (corr_ru-1)*v_r) - gamma;  %mu
Y = (v_mc - v_mer - corr_mef*v_mef - (corr_ru-1)*v_mu - (corr_ru-1)*v_r - gamma/beta) ./ v_mc; %Y

%% Plot allocation scheme for BW25113 - Fig 2A

fracGlc = [0.37,0.44,0.09,0.10]; %chi_u, chi_r, chi_c, chi_er+chi_ef
fracGly =[0.36,0.38,0.1,0.16];
fracGlc02 = [0.35,0.29,0.1,0.25];
fracGlc035 = [0.36,0.3,0.1,0.23];
fracGlc05 = [0.34,0.36,0.1,0.19];

chi = [fracGlc02;fracGlc035;fracGlc05;fracGly;fracGlc]';

figure;
chir = bar(chi');
chir(1).FaceColor = [0,0,1]; %blue
chir(2).FaceColor = [1 0 0];%red
chir(4).FaceColor = [0.0,0.65,0]; %dark green
chir(3).FaceColor = [0.95    0.6940    0.1250]; %yellow
xticklabels({'D0.2','D0.35','D0.5', 'Gly','Glc'});
legend('\chi_u','\chi_r', '\chi_c', '\chi_{er}+\chi_{ef}', 'Location','eastoutside');
xlabel('Growth condition');
ylabel('Resource allocation')



%% Plot rate-yield plane and data points - Fig 2B-C
if strcmp(scenario, 'gly') 
      data = xlsread('Tradeoff_data_gly_paper.xlsx', 'Batch');
elseif strcmp(scenario, 'glc') || strcmp(scenario, 'glc_varying_parameters') || strcmp(scenario, 'glc_MG')
         data = xlsread('Tradeoff_data_glc_paper.xlsx', 'Batch');

         % Conversion from OD600 to gDW ( see Basan)
         data(96:109,4) = data(96:109,4) / 0.49;  %Glc uptake flux
         data(96:109,5) = data(96:109,5) / 0.49;  %acetate overflow flux


         %conversion from Cmmol to mmol
         data(229:232,4) = data(229:232,4) / 6;
         data(229:232,5) = data(229:232,5) / 2; 
end

if strcmp(scenario, 'glc') || strcmp(scenario, 'glc_varying_parameters') || strcmp(scenario, 'gly') || strcmp(scenario, 'glc_MG')

    figure; hold on;
    plot(mu,Y,'LineStyle','none','Marker','o','MarkerSize',3, 'Color',[0.8,0.8,0.8]);
    if strcmp(scenario, 'gly')
        WTIndices = [5, 9, 11:12, 17:20]-1;
        scatter(data(WTIndices,1), data(WTIndices,2),'ob','fill'); %other WT strains
        mutIndices = [3:4, 6:8, 10, 13:16]-1;
        scatter(data(mutIndices,1), data(mutIndices,2),'or','fill'); %mutant strains
        ALEIndices = [21:26]-1;
        scatter(data(ALEIndices,1), data(ALEIndices,2),'o','MarkerFaceColor',[0.0,0.65,0],'MarkerEdgeColor','none'); %ALE strains
        scatter(data(1,1), data(1,2),'ok','fill'); %WT calibration
        legend('Model','Wild-type','Mutant','ALE','Calibration','Location', 'SouthEast');
        title('Gly');
    elseif strcmp(scenario, 'glc') || strcmp(scenario, 'glc_varying_parameters')|| strcmp(scenario, 'glc_MG')
        WTIndices = [99, 111:119, 121, 126,  128:138, 148:150, 195:198, 206:207,211:222,230 ]-1;      
        scatter(data(WTIndices,1), data(WTIndices,2),'ob','fill'); %other WT strains
        mutIndices = [3:98,100:110,120, 122:125, 127, 139:147, 199:205, 208:210, 223:229, 231:233]-1;   
        scatter(data(mutIndices,1), data(mutIndices,2),'or','fill'); %mutant strains
        ALEIndices = [151:194]-1;
        scatter(data(ALEIndices,1), data(ALEIndices,2),'o','MarkerFaceColor',[0.0,0.65,0],'MarkerEdgeColor','none'); %ALE strains
        scatter(data(1,1), data(1,2),'ok','fill'); %WT calibration
        legend('Model','Wild-type','Mutant','ALE','Calibration','Location', 'SouthEast');
        title('Glc');
    end
    
    if strcmp(scenario, 'glc_varying_parameters')
        xlim([0,1.5]);
    else
        xlim([0,1.3]);
    end
    ylim([0,1]);
    xlabel('Growth rate (h^{-1})');
    ylabel('Growth yield');

end

%% Plot two-dimensional tradeoffs

if strcmp(scenario, 'glc') || strcmp(scenario, 'glc_varying_parameters') || strcmp(scenario, 'gly')

    available_ace = not(isnan(data(:,5)));
    available_glc = not(isnan(data(:,4)));
    
    is_WT = ismember([1:length(data(:,1))], WTIndices)';
    is_mut = ismember([1:length(data(:,1))], mutIndices)';
    is_ALE = ismember([1:length(data(:,1))], ALEIndices)';
    
   
    % Plot data
    figure; hold on;
    plot(v_mc,Y,'LineStyle','none','Marker','o','MarkerSize',3, 'Color',[0.8,0.8,0.8]);
     ylim([0,1]);
    xlim([0,max(v_mc)]);
    ylabel('Growth yield');
    xlabel('Glucose uptake rate (Cmmol gDW^{-1} h^{-1})');
    if strcmp(scenario, 'glc')|| strcmp(scenario, 'glc_varying_parameters')
        
        scatter(6*data(available_glc&is_WT,4),data(available_glc&is_WT,2),'ob','fill'); 
        scatter(6*data(available_glc&is_mut,4),data(available_glc&is_mut,2),'or','fill'); 
        scatter(6*data(available_glc&is_ALE,4),data(available_glc&is_ALE,2),'o','MarkerFaceColor',[0.0,0.65,0],'MarkerEdgeColor','none'); 
        scatter(6*data(1,4),data(1,2),'ok','fill');
          legend('Model','Wild-type','Mutant','ALE','Calibration','Location', 'NorthEast');
    else
        legend('Model','Location', 'NorthEast');      

    end
   
    
     

figure;hold on;
plot(v_mc, mu,'LineStyle','none','Marker','o','MarkerSize',3,'Color',[0.8,0.8,0.8] );
if strcmp(scenario, 'glc')|| strcmp(scenario, 'glc_varying_parameters')
        scatter(6*data(available_glc&is_WT,4),data(available_glc&is_WT,1),'ob','fill'); 
        scatter(6*data(available_glc&is_mut,4),data(available_glc&is_mut,1),'or','fill'); 
        scatter(6*data(available_glc&is_ALE,4),data(available_glc&is_ALE,1),'o','MarkerFaceColor',[0.0,0.65,0],'MarkerEdgeColor','none'); 
        scatter(6*data(1,4),data(1,1),'ok','fill');
         legend('Model','Wild-type','Mutant','ALE','Calibration','Location', 'SouthEast');
else
        legend('Model','Location', 'NorthEast');  
end

ylabel('Growth rate (h^{-1})');
xlabel('Glucose uptake rate (Cmmol gDW^{-1} h^{-1})');
ylim([0,1.2]);
xlim([0,max(v_mc)]);
%legend('Model','WT calibration','Mutant', 'Wild-type','ALE','Location', 'SouthEast');
 

figure;hold on;
plot(v_mef, mu,'LineStyle','none','Marker','o','MarkerSize',3,'Color',[0.8,0.8,0.8] );
if strcmp(scenario, 'glc')|| strcmp(scenario, 'glc_varying_parameters')
         scatter(2*data(available_ace&is_WT,5),data(available_ace&is_WT,1),'ob','fill'); 
         scatter(2*data(available_ace&is_mut,5),data(available_ace&is_mut,1),'or','fill'); 
        scatter(2*data(available_ace&is_ALE,5),data(available_ace&is_ALE,1),'o','MarkerFaceColor',[0.0,0.65,0],'MarkerEdgeColor','none'); 
         scatter(2*data(1,5),data(1,1),'ok','fill');
         legend('Model','Wild-type','Mutant','ALE','Calibration','Location', 'SouthEast');
 else
        legend('Model','Location', 'NorthEast');  
end
ylabel('Growth rate (h^{-1})');
xlabel('Acetate secretion rate (Cmmol gDW^{-1} h^{-1})');
ylim([0,1.2]);
xlim([0, 40])
 

figure;hold on;
plot(v_mef,Y,'LineStyle','none','Marker','o','MarkerSize',3,'Color',[0.8,0.8,0.8] );
if strcmp(scenario, 'glc')|| strcmp(scenario, 'glc_varying_parameters')
        scatter(2*data(available_ace&is_WT,5),data(available_ace&is_WT,2),'ob','fill'); 
        scatter(2*data(available_ace&is_mut,5),data(available_ace&is_mut,2),'or','fill'); 
        scatter(2*data(available_ace&is_ALE,5),data(available_ace&is_ALE,2),'o','MarkerFaceColor',[0.0,0.65,0],'MarkerEdgeColor','none'); 
        scatter(2*data(1,5),data(1,2),'ok','fill');
         legend('Model','Wild-type','Mutant','ALE','Calibration','Location', 'NorthEast');
      else
        legend('Model','Location', 'NorthEast');     
    end
ylabel('Growth yield');
xlabel('Acetate secretion rate (Cmmol gDW^{-1} h^{-1})');
ylim([0,1]);
%   


figure; hold on;
plot(v_mc, v_mef,'LineStyle','none','Marker','o','MarkerSize',3,'Color',[0.8,0.8,0.8] );
ylabel('Acetate secretion rate (Cmmol gDW^{-1} h^{-1})');
xlabel('Glucose uptake rate (Cmmol gDW^{-1} h^{-1})');
xlim([0,max(v_mc)]);
if strcmp(scenario, 'glc')|| strcmp(scenario, 'glc_varying_parameters')
         scatter(6*data(available_glc&is_WT,4),2*data(available_glc&is_WT,5),'ob','fill'); 
        scatter(6*data(available_glc&is_mut,4),2*data(available_glc&is_mut,5),'or','fill'); 
        scatter(6*data(available_glc&is_ALE,4),2*data(available_glc&is_ALE,5),'o','MarkerFaceColor',[0.0,0.65,0],'MarkerEdgeColor','none'); 
          scatter(6*data(1,4),2*data(1,5),'ok','fill');
           legend('Model','Wild-type','Mutant','ALE','Calibration','Location', 'NorthWest');
else
        legend('Model','Location', 'NorthEast');  
end
   
end






%% Plot chemostat results - Fig 2D

if strcmp(scenario, 'chemostat')

    %Load experimental data
    Exdata = xlsread('./Tradeoff_data_glc_paper.xlsx', 'Chemostat');

    mutIndices = [109:111,120:122, 128:131]-1;  
    mutIndices= mutIndices';

    %Load simulation results

    load('Sampling_GlcD035_variableMu.mat');
    v_mc1 = steady_flux(:,1);
    v_mer1 = steady_flux(:,2);
    v_mef1 = steady_flux(:,3);
    v_r1 = steady_flux(:,4);
    v_mu1 = steady_flux(:,5);
    mu1 = beta * (v_mc1 - v_mer1 - corr_mef*v_mef1 - (corr_ru-1)*v_mu1 - (corr_ru-1)*v_r1) - gamma;
    Y1 = (v_mc1 - v_mer1 - corr_mef*v_mef1 - (corr_ru-1)*v_mu1 - (corr_ru-1)*v_r1 - gamma/beta)./v_mc1 ;
     
    load('Sampling_GlcD05_variableMu.mat');
 %   load(Sampling_GlcD05.mat');


    v_mc2 = steady_flux(:,1);
    v_mer2 = steady_flux(:,2);
    v_mef2 = steady_flux(:,3);
    v_r2 = steady_flux(:,4);
    v_mu2 = steady_flux(:,5);
    
    mu2 = beta * (v_mc2 - v_mer2 - corr_mef*v_mef2 - (corr_ru-1)*v_mu2 - (corr_ru-1)*v_r2) - gamma;
    Y2 = (v_mc2 - v_mer2 - corr_mef*v_mef2 - (corr_ru-1)*v_mu2 - (corr_ru-1)*v_r2 - gamma/beta)./v_mc2 ;
    
    %plot
    mu_target = 0.2;
    delta = 0.03;
    sel = (mu > mu_target-delta) & (mu < mu_target+delta);
    sel_data = (Exdata(:,1) > mu_target-delta) & (Exdata(:,1) < mu_target+delta);
    mut02 = (Exdata(mutIndices,1) > mu_target-delta) & (Exdata(mutIndices,1) < mu_target+delta);
    
    mu_target = 0.35;
    sel1 = (mu1 > mu_target-delta) & (mu1 < mu_target+delta);
    sel_data1 = (Exdata(:,1) > mu_target-delta) & (Exdata(:,1) < mu_target+delta);
    mut035 = (Exdata(mutIndices,1) > mu_target-delta) & (Exdata(mutIndices,1) < mu_target+delta);
    
    mu_target = 0.5;
    sel2 = (mu2 > mu_target-delta) & (mu2 < mu_target+delta);
    sel_data2 = (Exdata(:,1) > mu_target-delta) & (Exdata(:,1) < mu_target+delta);
    
    figure;
    p1 = plot(mu(sel),Y(sel),'LineStyle','none','Marker','o','MarkerSize',3, 'Color',[0.8,0.8,0.8]);hold on;
    plot(mu1(sel1),Y1(sel1),'LineStyle','none','Marker','o','MarkerSize',3, 'Color',[0.8,0.8,0.8]);hold on;
    plot(mu2(sel2),Y2(sel2),'LineStyle','none','Marker','o', 'MarkerSize',3, 'Color',[0.8,0.8,0.8]);hold on;
    p2 = scatter(Exdata(sel_data|sel_data1|sel_data2,1), Exdata(sel_data|sel_data1|sel_data2,2),'ob', 'fill'); %other WT
    p3 = scatter (Exdata(1:3,1),Exdata(1:3,2), 'ok', 'fill'); %BW, calibration
    p4 = scatter (Exdata(mutIndices(mut035|mut02),1),Exdata(mutIndices(mut02|mut035),2), 'or', 'fill'); %mutants
    legend([p1,p2,p4,p3], {'Model','Wild-type','Mutant','Calibration'});
    xlim([0,1.2]);
    ylim([0,1]);
    xlabel('Growth rate (h^{-1})');
    ylabel('Growth yield');
end


%% Plot rate-yield plane and relative acetate overflow, maintenance costs, glc uptake - Fig S3
 if strcmp(scenario, 'glc') || strcmp(scenario, 'glc_varying_parameters') || strcmp(scenario, 'gly')

    figure; hold on;
    scatter(mu, Y, 8, v_mc, 'fill');
    if strcmp(scenario, 'glc_varying_parameters')
        xlim([0,1.5]);
    else
        xlim([0,1.2]);
    end
    ylim([0,1]);
    xlabel('Growth rate (h^{-1})');
    ylabel('Growth yield');
    %caxis manual;
    %caxis([0 0.35]);
    colorbar;
    title('Glucose uptake rate');


    figure; hold on;
    scatter(mu, Y, 8, gamma./v_mc, 'fill');
    if strcmp(scenario, 'glc_varying_parameters')
        xlim([0,1.5]);
    else
        xlim([0,1.2]);
    end
    ylim([0,1]);
    xlabel('Growth rate (h^{-1})');
    ylabel('Growth yield');
    caxis manual;
    caxis([0 0.005]);
    colorbar;
    title('Relative weigth of maintenance costs');

 



figure;hold on;
plot(v_mef./v_mc, mu,'LineStyle','none','Marker','o','MarkerSize',3,'Color',[0.8,0.8,0.8] );
ylabel('Growth rate (h^{-1})');
xlabel('Relative acetate secretion rate');
ylim([0,1.2]);
xlim([0,0.7]);
if strcmp(scenario, 'glc')|| strcmp(scenario, 'glc_varying_parameters')
         scatter(2*data(available_glc&available_ace&is_WT,5)./(6*data(available_glc&available_ace&is_WT,4))  ,data(available_glc&available_ace&is_WT,1),'ob','fill'); 
         scatter(2*data(available_glc&available_ace&is_mut,5)./(6*data(available_glc&available_ace&is_mut,4)),data(available_glc&available_ace&is_mut,1),'or','fill'); 
         scatter(2*data(available_glc&available_ace&is_ALE,5)./(6*data(available_glc&available_ace&is_ALE,4)) ,data(available_glc&available_ace&is_ALE,1),'o','MarkerFaceColor',[0.0,0.65,0],'MarkerEdgeColor','none'); 
         scatter(2*data(1,5)/(6*data(1,4)),data(1,1),'ok','fill');
  legend('Model','Wild-type','Mutant','ALE','Calibration','Location', 'NorthEast');
else
   legend('Model','Location', 'NorthEast');  
 end

   

figure;hold on;
plot(v_mef./v_mc,Y,'LineStyle','none','Marker','o','MarkerSize',3,'Color',[0.8,0.8,0.8] );
if strcmp(scenario, 'glc')|| strcmp(scenario, 'glc_varying_parameters')
        scatter(2*data(available_glc&available_ace&is_WT,5)./(6*data(available_glc&available_ace&is_WT,4)),data(available_glc&available_ace&is_WT,2),'ob','fill'); 
        scatter(2*data(available_glc&available_ace&is_mut,5)./(6*data(available_glc&available_ace&is_mut,4)),data(available_glc&available_ace&is_mut,2),'or','fill'); 
        scatter(2*data(available_glc&available_ace&is_ALE,5)./(6*data(available_glc&available_ace&is_ALE,4)),data(available_glc&available_ace&is_ALE,2),'o','MarkerFaceColor',[0.0,0.65,0],'MarkerEdgeColor','none'); 
        scatter(2*data(1,5)/(6*data(1,4)),data(1,2),'ok','fill');
end
ylabel('Growth yield');
xlabel('Relative acetate secretion rate');
ylim([0,1]);
xlim([0,0.7]);
if strcmp(scenario, 'glc')|| strcmp(scenario, 'glc_varying_parameters')
    legend('Model','Wild-type','Mutant','ALE','Calibration','Location', 'NorthEast');
else
    legend('Model','Location', 'NorthEast');
end
   
 end




%% Plot relation between rate, yield, fluxes and resource allocation parameters in 3D representation for selected values of chi_er - Fig 4 

if strcmp(scenario, 'glc_3D')

    selection = (mu>0);  %mu>0
    chi_er_1 = 0.02;
    chi_er_2 = 0.1;
    chi_er_3 = 0.2;
    coord_chi_er_1 = findPoints(chi_er, chi_er_1, 0.01);
    coord_chi_er_2 = findPoints(chi_er, chi_er_2, 0.01);
    coord_chi_er_3 = findPoints(chi_er, chi_er_3, 0.01);
    ind_chi_er_1 = zeros(length(mu),1); ind_chi_er_1(coord_chi_er_1) = 1;
    ind_chi_er_2 = zeros(length(mu),1); ind_chi_er_2(coord_chi_er_2) = 1;
    ind_chi_er_3 = zeros(length(mu),1); ind_chi_er_3(coord_chi_er_3) = 1;

    figure; hold on;
    scatter3(chi_r(selection&ind_chi_er_1), chi_c(selection&ind_chi_er_1), chi_er(selection&ind_chi_er_1),[], mu(selection&ind_chi_er_1),'fill','SizeData',3);
    scatter3(chi_r(selection&ind_chi_er_2), chi_c(selection&ind_chi_er_2), chi_er(selection&ind_chi_er_2),[], mu(selection&ind_chi_er_2),'fill','SizeData',3);
    scatter3(chi_r(selection&ind_chi_er_3), chi_c(selection&ind_chi_er_3), chi_er(selection&ind_chi_er_3),[], mu(selection&ind_chi_er_3),'fill','SizeData',3);
    colorbar;
    axis square;
    grid on;
    xlim([0 0.6]);  
    ylim([0 0.6]);
    zlim([0 0.2]);
    title('Growth rate (h^{-1})'); 
    xlabel('Allocation parameter \it \chi_r'); ylabel('Allocation parameter \it \chi_c'); zlabel('Allocation parameter \it \chi_{er}')
    ax = gca;
    ax.FontSize = 12;

    figure; hold on;
    scatter3(chi_r(selection&ind_chi_er_1), chi_c(selection&ind_chi_er_1), chi_er(selection&ind_chi_er_1),[], Y(selection&ind_chi_er_1),'fill','SizeData',3);
    scatter3(chi_r(selection&ind_chi_er_2), chi_c(selection&ind_chi_er_2), chi_er(selection&ind_chi_er_2),[], Y(selection&ind_chi_er_2),'fill','SizeData',3);
    scatter3(chi_r(selection&ind_chi_er_3), chi_c(selection&ind_chi_er_3), chi_er(selection&ind_chi_er_3),[], Y(selection&ind_chi_er_3),'fill','SizeData',3);
    colorbar;
    axis square;
    grid on;
    xlim([0 0.6]);  
    ylim([0 0.6]);
    zlim([0 0.2]);
    title('Growth yield'); 
    xlabel('Allocation parameter \it \chi_r'); ylabel('Allocation parameter \it \chi_c'); zlabel('Allocation parameter \it \chi_{er}')
    ax = gca;
    ax.FontSize = 12;

    figure; hold on;
    scatter3(chi_r(selection&ind_chi_er_1), chi_c(selection&ind_chi_er_1), chi_er(selection&ind_chi_er_1),[], v_mc(selection&ind_chi_er_1),'fill','SizeData',3);
    scatter3(chi_r(selection&ind_chi_er_2), chi_c(selection&ind_chi_er_2), chi_er(selection&ind_chi_er_2),[], v_mc(selection&ind_chi_er_2),'fill','SizeData',3);
    scatter3(chi_r(selection&ind_chi_er_3), chi_c(selection&ind_chi_er_3), chi_er(selection&ind_chi_er_3),[], v_mc(selection&ind_chi_er_3),'fill','SizeData',3);
    colorbar;
    axis square;
    grid on;
    xlim([0 0.6]);  
    ylim([0 0.6]);
    zlim([0 0.2]);
    title('Glucose uptake rate (Cmmol gDW^{-1} h^{-1})'); 
    xlabel('Allocation parameter \it \chi_r'); ylabel('Allocation parameter \it \chi_c'); zlabel('Allocation parameter \it \chi_{er}')


    figure; hold on;
    scatter3(chi_r(selection&ind_chi_er_1), chi_c(selection&ind_chi_er_1), chi_er(selection&ind_chi_er_1),[], v_mef(selection&ind_chi_er_1)./v_mc(selection&ind_chi_er_1),'fill','SizeData',3);
    scatter3(chi_r(selection&ind_chi_er_2), chi_c(selection&ind_chi_er_2), chi_er(selection&ind_chi_er_2),[], v_mef(selection&ind_chi_er_2)./v_mc(selection&ind_chi_er_2),'fill','SizeData',3);
    scatter3(chi_r(selection&ind_chi_er_3), chi_c(selection&ind_chi_er_3), chi_er(selection&ind_chi_er_3),[], v_mef(selection&ind_chi_er_3)./v_mc(selection&ind_chi_er_3),'fill','SizeData',3);
    colorbar;
    axis square;
    grid on;
    xlim([0 0.6]);  
    ylim([0 0.6]);
    zlim([0 0.2]);
    title('Relative acetate secretion rate'); 
    xlabel('Allocation parameter \it \chi_r'); ylabel('Allocation parameter \it \chi_c'); zlabel('Allocation parameter \it \chi_{er}')
    
end

%% Plot rate-yield curves for single strain in different media - Fig S3
% Data from different authors

data_others = xlsread('dataRateYield_others.xlsx');

data_BasanCheng_mu = data_others(:,1);
[~,I_BasanCheng] = sort(data_BasanCheng_mu);
data_BasanCheng_v_glc = data_others(:,2);
data_BasanCheng_v_ace = data_others(:,3);

data_Nanchen_mu = data_others(:,4);
[~,I_Nanchen] = sort(data_Nanchen_mu);
data_Nanchen_v_ace_div_v_glc = data_others(:,5);

data_Holms_mu = data_others(:,6);
[~,I_Holms] = sort(data_Holms_mu);
data_Holms_v_glc = data_others(:,7);
data_Holms_v_ace = data_others(:,8);

data_Gerosa_mu = data_others(:,9);
[~,I_Gerosa] = sort(data_Gerosa_mu);
data_Gerosa_v_glc = data_others(:,10);
data_Gerosa_v_ace = data_others(:,11);

data_Valgepea_mu = data_others(:,12);
[~,I_Valgepea] = sort(data_Valgepea_mu);
data_Valgepea_conc_glc = data_others(:,13);
data_Valgepea_conc_ace = data_others(:,14);
data_Valgepea_feed_glc = data_others(:,15);

data_Peebo_mu = data_others(:,16);
[~,I_Peebo] = sort(data_Peebo_mu);
data_Peebo_C_ace = data_others(:,17);

figure; hold on;
plot(data_BasanCheng_mu(I_BasanCheng), (2/6)*data_BasanCheng_v_ace(I_BasanCheng)./data_BasanCheng_v_glc(I_BasanCheng),...
    'LineStyle','-','Marker','o','Color','r','MarkerEdgeColor','r','MarkerFaceColor','r');
plot(data_Nanchen_mu(I_Nanchen), (2/6)*(59.04/180.156)*data_Nanchen_v_ace_div_v_glc(I_Nanchen),...
    'LineStyle','-','Marker','o','Color','b','MarkerEdgeColor','b','MarkerFaceColor','b');
plot(data_Holms_mu(I_Holms), (2/6)*data_Holms_v_ace(I_Holms)./data_Holms_v_glc(I_Holms),...
    'LineStyle','-','Marker','o','Color','g','MarkerEdgeColor','g','MarkerFaceColor','g');
plot(data_Gerosa_mu(I_Gerosa), (2/6)*data_Gerosa_v_ace(I_Gerosa)./data_Gerosa_v_glc(I_Gerosa),...
    'LineStyle','-','Marker','o','Color','k','MarkerEdgeColor','k','MarkerFaceColor','k');
plot(data_Valgepea_mu(I_Valgepea), (2/6)* data_Valgepea_conc_ace(I_Valgepea)./((1000/180.156)*data_Valgepea_feed_glc(I_Valgepea) - data_Valgepea_conc_glc(I_Valgepea)),...
    'LineStyle','-','Marker','o','Color','c','MarkerEdgeColor','c','MarkerFaceColor','c');
plot(data_Peebo_mu(I_Peebo), data_Peebo_C_ace(I_Peebo)/100,...
    'LineStyle','-','Marker','o','Color','m','MarkerEdgeColor','m','MarkerFaceColor','m');
xlim([0,1.2]);
ylim([0,0.4]);
xlabel('Growth rate (h^{-1})');
ylabel('Relative acetate secretion rate');
legend('Basan \it et al. \rm / Cheng \it et al.', '\rm Nanchen \it et al.', 'Holms', '\rm Gerosa \it et al.', '\rm Valgepea \it et al.', '\rm Peebo \it et al.');

%% Plot contour level for mu=0.61 and Y=0.5, at fixed chi_e - Fig S4
if  strcmp(scenario, 'glc_grid')
    n = sqrt(size(mu,1));
    npoint = n+1;
    
    z_mu = zeros(n);
    z_mu(logical(ones(n))) = mu;
    
    z_Y = zeros(n);
    z_Y(logical(ones(n))) = Y; 
    
    r1 =1/npoint:1/npoint: (npoint-1)/npoint;
    c1=1/npoint:1/npoint: (npoint-1)/npoint;
    
    figure;  
    contour(r1,c1,z_mu, [0.61,0.61], 'm', 'LineWidth',2);
    hold on; contour(r1,c1,z_Y, [0.5,0.5],'LineWidth',2);
    xlabel('Allocation parameter \chi_r');
    ylabel('Allocation parameter \chi_c');
    xlim([0,0.6]); ylim([0,0.6])
end

%% Determine predicted and observed allocation profiles for NCM3722  strain

if strcmp(scenario, 'glc_NCM')

    % Determine observed phenotype for NCM3722 strain
    muMeas = 0.97;
    YMeas = 0.6; % Cheng et al., 2019
    protFrac_NCM=0.73;
    cFrac_NCM=0.026;
        
    % Estimate chi's for NCM3722
    chi_u_NCM = 0.35;
    chi_r_NCM = 0.48;
    chi_c_NCM = 0.07;
    chi_e_NCM = 0.10;

     v_glc = 5.4 / 0.49;  %Cheng et al., 2019 and conversion mmol gDW-1 h-1, using value of Basan et al.
     v_ace = 2.52 / 0.49; 


    v_mc_Meas = 6 * v_glc;
    v_mef_Meas = 2 * v_ace;
    eta = 7.2; % Cmmol gDW-1 , growth-rate-dependent CO2 loss for biomass synthesis, Fischer et al. and Basan et al.
    v_rq_CO2 = eta * muMeas; % Cmmol gDW-1 h-1 
           
    % inferred respiration rate
    v_CO2_tot = v_mc_Meas - v_mef_Meas - (muMeas + gamma) * (1/beta); %Cmmol gDW-1 h-1
    v_mef_CO2 = (corr_mef - 1) * v_mef_Meas;
    v_mer_Meas = v_CO2_tot - v_mef_CO2 - v_rq_CO2;
    
    mer_fraction = 0.58 + (0.29 + 0.12) * (v_mer_Meas/(v_mer_Meas + v_mef_Meas)); % NCM3722, data Schmidt et al.
    mef_fraction = 0.02 + (0.29 + 0.12) * (v_mef_Meas/(v_mer_Meas + v_mef_Meas)); % NCM3722, data Schmidt et al.
    chi_er_NCM = mer_fraction * chi_e_NCM;
        
    % Find points (cand_NCM) close to expected mu, Y,v_mc, v_mef values for NCM strain
    % within an error d
    
    d = 0.05; % admissible error margin (percentage) 

    [cand_NCM, best_NCM] = findPoints_strict([mu, Y, v_mc, v_mef],[muMeas, YMeas, v_mc_Meas, v_mef_Meas], d);
    
end

%% Boxplot NCM candidates : Model with constant param vs Model with variable parameters

if strcmp(scenario, 'glc_NCM') 
 mean_NCM=[chi_u_NCM, chi_r_NCM, chi_c_NCM ,chi_e_NCM, protFrac_NCM, cFrac_NCM];

    if ~isempty(cand_NCM)
     
        D=[chi_u(cand_NCM); chi_r(cand_NCM); chi_c(cand_NCM);chi_e(cand_NCM);];
        B= [ p(cand_NCM)*beta; c(cand_NCM)*beta];

        l=size(cand_NCM,2);
         label_biomass=[repmat({'p\beta'},l,1);repmat({'c\beta'},l,1)];
         label_chi=[ repmat({'\chi_u'},l,1); repmat({'\chi_r'},l,1);repmat({'\chi_c'},l,1);repmat({'\chi_e'},l,1); ];
      
        sampling= [repmat({'Model constant k'},l*4,1);];
        sampling_b= [repmat({'Model constant k'},l*2,1);];
     % x = 1:4;
     % xm=repmat(x,size(cand_NCM,2),1);
    end
   
  
load('Sampling_Glc_variableMu_variablek.mat');

v_mc = steady_flux(:,1);
v_mer = steady_flux(:,2);
v_mef = steady_flux(:,3);
v_r = steady_flux(:,4);
v_mu = steady_flux(:,5);

% Compute concentrations
r = steady_variables(:,1);
m_er = steady_variables(:,2);
m_ef = steady_variables(:,3);
m_c = steady_variables(:,4);
m_u = steady_variables(:,5);
c = steady_variables(:,6);
p= r+m_ef+m_er+m_c+m_u;

chi_u = steady_variables(:,9);
chi_r = steady_variables(:,10);
chi_c = steady_variables(:,11);  
chi_e = 1-chi_u -chi_r-chi_c;

% Compute rate and yield
mu = beta * (v_mc - v_mer - corr_mef*v_mef - (corr_ru-1)*v_mu - (corr_ru-1)*v_r) - gamma;  %mu
Y = (v_mc - v_mer - corr_mef*v_mef - (corr_ru-1)*v_mu - (corr_ru-1)*v_r - gamma/beta) ./ v_mc; %Y

[cand_NCM_k, best_NCM_k] = findPoints_strict([mu, Y, v_mc, v_mef],[muMeas, YMeas, v_mc_Meas, v_mef_Meas], d);

    if ~isempty(cand_NCM_k)      
     % x = 1:4;
     % xm_k=repmat(x,size(cand_NCM_k,2),1);

        Dk=[chi_u(cand_NCM_k); chi_r(cand_NCM_k); chi_c(cand_NCM_k);chi_e(cand_NCM_k);];
        Bk= [ p(cand_NCM_k)*beta; c(cand_NCM_k)*beta];
     
        lk=size(cand_NCM_k,2);
        label_biomass_k=[ repmat({'p\beta'},lk,1);repmat({'c\beta'},lk,1)];
        label_chi_k=[ repmat({'\chi_u'},lk,1); repmat({'\chi_r'},lk,1);repmat({'\chi_c'},lk,1);repmat({'\chi_e'},lk,1);];
       
        sampling_k= [repmat({'Model variable k'},lk*4,1);];
        sampling_b_k=  [repmat({'Model variable k'},lk*2,1);];

    end

% Plot prediction/data : allocation NCM

chi_u_NCM =[0.342,0.341,0.355]; 
chi_r_NCM= [0.485,0.487,0.474];
chi_c_NCM=[0.072,0.072,0.071];
chi_e_NCM=[0.101,0.100,0.100];
replicate_NCM= [chi_u_NCM'; chi_r_NCM'; chi_c_NCM' ;chi_e_NCM';];

sampling_data= repmat({'Data'},4*3,1);
label_data=[ repmat({'\chi_u'},3,1); repmat({'\chi_r'},3,1);repmat({'\chi_c'},3,1);repmat({'\chi_e'},3,1);];
      
x= [repmat(1,3,1);repmat(2,3,1);repmat(3,3,1); repmat(4,3,1)];
chi_order={'\chi_u','\chi_r','\chi_c','\chi_e'};


label2= categorical( cat(1,label_chi,label_chi_k),chi_order );
sampling_scheme2= categorical(cat(1,sampling,sampling_k));

figure;
b3=boxchart(label2,cat(1,D,Dk),'GroupByColor',sampling_scheme2); hold on;
s1=scatter(x'-0.25,replicate_NCM, 'ok', 'fill','SizeData',16, 'DisplayName', 'Data NCM'); hold on;
scatter(x'+0.25,replicate_NCM, 'ok', 'fill','SizeData',16, 'HandleVisibility','off'); 
[b3.MarkerStyle]=deal('none');  %no outliers
[b3.MarkerSize]=deal(1);
ax = gca;
ax.FontSize = 14;
%hold off;
ylabel('Fraction');

% Plot prediction/data : biomass composition NCM

biomass_NCM= [protFrac_NCM, cFrac_NCM];

x=1:2;
sampling_scheme2= categorical(cat(1,sampling_b,sampling_b_k));
label_order= {'p\beta','c\beta'};
label2= categorical( cat(1,label_biomass,label_biomass_k),label_order);

figure;
b4=boxchart(label2,cat(1,B,Bk),'GroupByColor',sampling_scheme2); hold on;
s1=scatter(x'-0.25,biomass_NCM, 'ok', 'fill','SizeData',16, 'DisplayName', 'Data NCM'); hold on;
scatter(x'+0.25,biomass_NCM, 'ok', 'fill','SizeData',16, 'HandleVisibility','off'); 
legend('Location','northeast');
%[b2.JitterOutliers]=deal('on');
%[b3.MarkerStyle]=deal('.');
[b4.MarkerStyle]=deal('none');  %no outliers
[b4.MarkerSize]=deal(0.8);
%hold off;
ax = gca;
ax.FontSize = 14;
ylabel('Fraction')

end

%% Make spider plot for kinetic parameters in candidate NCM solutions

% This script makes use of the spider_plot function developed by Moses 
% Moses (2022). spider_plot (https://github.com/NewGuy012/spider_plot/releases/tag/17.3), GitHub

if strcmp(scenario, 'glc_NCM')

    %Plot for NCM
     A = zeros(length(cand_NCM_k)+1,3);
    for i = 1:length(cand_NCM_k)
        A(i,:) = param_samples{cand_NCM_k(i),3};    
    end
    

   ref = [parameters.e_s,parameters.k_mer, parameters.k_mef]; %reference parameter values
    P = A./ref;
    P(length(cand_NCM_k)+1,:) = [1,1,1];

%     ref = [parameters.e_s,parameters.k_mer, parameters.k_mef, ratesConc.c]; %reference parameter values
%     B=[A, [c(cand_NCM);1]];
%     P=B./ref;
%     P(length(cand_NCM)+1,:) = [1,1,1,1];

    colorValue = round(P(1:length(cand_NCM_k),2)*10-3); %integer between 1 and 16
    colorVec = turbo(max(colorValue)); %number color levels
    figure;
   spider_plot_R2019b(P, 'AxesLimits', [0.5,0.5,0.5; 2,2,2], 'AxesLabels',{'k_{mc}','k_{mer}','k_{mef}'}, ...
       'LineWidth',[0.3*ones(1,length(cand_NCM_k)),1],'LabelFontSize',12, 'AxesFontSize',12, 'Marker','none', ...
       'Color',[colorVec(colorValue,:); 0,0,0],'AxesLabelsOffset',0.15); %relative change with respect to standard values
    
    title('NCM3722', 'FontSize', 16);
    
end
%% barplot phenotypic data BW, NCM
if strcmp(scenario, 'glc_NCM') 

mu_all= [0.61,0.97]; %BW, NCM
Y_all= [0.5,0.6];


v_mc_all =[49.6, 66.1];
v_mef_all= [9.8,10.3];

c_frac_all= [0.009,0.026];
p_frac_all=[0.74,0.73];

phenotype= [mu_all; Y_all; v_mc_all; v_mef_all; c_frac_all; p_frac_all];
phenotype2= [mu_all; Y_all;  nan(1,2); nan(1,2);];
fluxes= [ nan(1,2); nan(1,2);v_mc_all; v_mef_all; ];

x=1:4;

figure;
yyaxis left
u=bar(x,phenotype2);
u(1).FaceColor = [0,0,1]; %blue
u(2).FaceColor='#A2142F';%dark red
u(2).FaceAlpha = 0.9;
%legend('BW25113, NCM3722');
xticklabels({'\mu','Y','v_{mc}','v_{mef}'});
ylabel('Growth rate (h^{-1}) and yield')
ax= gca;
ax.YColor= 'k';
ax.FontSize = 14;

yyaxis right
f=bar(x, fluxes);
f(1).FaceColor= [0,0,1]; %blue
f(2).FaceColor='#A2142F';%dark red
f(2).FaceAlpha = 0.9;
legend('BW25113', 'NCM3722','Location', 'north');
ylabel({'Uptake and secretion', ' rates (Cmmol gDW^{-1} h^{-1})'});
ax= gca;
ax.YColor= 'k';

hold off;
end

%% Plot BW/NCM biomass composition
if strcmp(scenario, 'glc_NCM') 
data= [0.72,0.02; 
       0.67,0.06
       NaN, NaN];

data3=[NaN,NaN,0.009,0.02];


data_class={
    'c'   'c1'   
    'g'   'g1'  
     'c'   ''
     'g'  ''};

data(end+1,:)=NaN;

fg=figure;
figure(fg);
yyaxis left
b2={bar(data([1 end],:),'stacked')};
hold on 
%xticklabels({'p\beta','p\beta', 'c\beta'})
ylabel('Protein fraction');
ax= gca;
ax.YColor= 'k';
for k=2:(size(data,1)-1)
    b2{k}=bar([k-1 k],data([end k],:),'stacked');
end
for k=1:(size(data,1)-1)
    for k2=1:size(data,2)
        switch data_class{k,k2}
            case 'c'
                set(b2{k}(k2),'FaceColor','b')
            case 'c1'
                set(b2{k}(k2),'FaceColor','w')
                hatchfill2(b2{k}(k2),'single','HatchAngle',45,'hatchcolor','b','hatchlinewidth',1.5);
            case 'g'
                set(b2{k}(k2),'FaceColor','#A2142F') %dark red
             case 'g1'
              %  set(b{k}(k2),'FaceColor','#CC0000')  
              set(b2{k}(k2),'FaceColor','w') %dark red
              hatchfill2(b2{k}(k2),'single','HatchAngle',45,'hatchcolor',[0.6350 0.0780 0.1840], 'hatchlinewidth',1.5);
        end
    end
end
yyaxis right
f2=bar(data3);
f2.FaceColor= 'flat';
f2.CData(3,:)=[0,0,1]; %blue
f2.CData(4,:)=[0.6350 0.0780 0.1840];%dark red
legend('BW25113', 'NCM3722','Location', 'northeast');
ylabel('Metabolite fraction'); 
ylim([0,0.025]);
%xticklabels({'','', 'c\beta'})
ax= gca;
ax.YColor= 'k';
%hold off;
 
end


%% Overlapping clouds mu-Y
if strcmp(scenario, 'glc_MG')

%load('Sampling_Glc_Km12.mat')
load('Sampling_Glc_Km12_variableMu.mat');


% Compute fluxes (If same parameter gamma?)
v_mc1 = steady_flux(:,1);
v_mer1 = steady_flux(:,2);
v_mef1 = steady_flux(:,3);
v_r1 = steady_flux(:,4);
v_mu1 = steady_flux(:,5);

mu1 = beta * (v_mc1 - v_mer1 - corr_mef*v_mef1 - (corr_ru-1)*v_mu1 - (corr_ru-1)*v_r1) - gamma;  %mu
Y1 = (v_mc1 - v_mer1 - corr_mef*v_mef1 - (corr_ru-1)*v_mu1 - (corr_ru-1)*v_r1 - gamma/beta) ./ v_mc1; %Y

figure;
plot(mu1,Y1,'LineStyle','none','Marker','.','MarkerSize',10, 'Color','b');
hold on;
plot(mu,Y,'LineStyle','none','Marker','.','MarkerSize',10, 'Color',[0.8,0.8,0.8]);
xlim([0,1.3]);
ylim([0,1]);
hold on;
xlabel('Growth rate (h^{-1})');
ylabel('Growth yield');
legend('BW25113','MG1655');

end

%% Relation between protein concentrations and growth rate along Pareto front

if strcmp(scenario, 'glc')

%[pareto_membership,pareto_point]=find_pareto_frontier([-mu,-Y]);
    
load('Pareto_Glc.mat');


figure; hold on;
plot(mu(pareto_membership), m_c(pareto_membership),'LineStyle','none','Marker','.','Color',[1,0.84,0],'MarkerSize',8);
plot(mu(pareto_membership), m_er(pareto_membership),'LineStyle','none','Marker','.','Color','g','MarkerSize',8);
plot(mu(pareto_membership), r(pareto_membership),'LineStyle','none','Marker','.','Color','r','MarkerSize',8);
plot(mu(pareto_membership), m_u(pareto_membership),'LineStyle','none','Marker','.','Color','b','MarkerSize',8);
plot(mu(pareto_membership), c(pareto_membership),'LineStyle','none','Marker','.','Color','c','MarkerSize',8);
plot(mu(pareto_membership), u(pareto_membership),'LineStyle','none','Marker','.','Color','m','MarkerSize',8);
plot(mu(pareto_membership), p(pareto_membership),'LineStyle','none','Marker','.','Color','k','MarkerSize',8);
legend('m_c','m_{er}', 'r', 'm_u', 'c', 'u','p');
xlabel('\mu (h^{-1})','FontSize',14);
ylabel('Concentrations (Cmmol gDW^{-1})','FontSize',14);



figure; hold on;
plot(mu(pareto_membership), c(pareto_membership)./(c(pareto_membership) + K_r),'LineStyle','none','Marker','.','Color','c','MarkerSize',8);
plot(mu(pareto_membership), as(pareto_membership)./(as(pareto_membership) + K_ar),'LineStyle','none','Marker','.','Color','b','MarkerSize',8);
plot(mu(pareto_membership), (a0-as(pareto_membership))./(a0-as(pareto_membership) + K_amer),'LineStyle','none','Marker','.','Color','r','MarkerSize',8);
plot(mu(pareto_membership), (parameters.corr_rq-1)*( v_r(pareto_membership)+v_mu(pareto_membership))./v_mc(pareto_membership),'LineStyle','none','Marker','.','Color','k','MarkerSize',8);

legend('c / (c + K_r)','a^* / (a^* + K_{ar})','(a0 - a^*) / (a0 - a^* + K_{amer})', 'v_{CO_2}/v_{mc}');
xlabel('\mu (h^{-1})','FontSize',14);


figure; hold on;
plot(mu(pareto_membership), chi_c(pareto_membership),'LineStyle','none','Marker','.','Color',[1,0.84,0],'MarkerSize',8);
plot(mu(pareto_membership), chi_er(pareto_membership),'LineStyle','none','Marker','.','Color','g','MarkerSize',8);
plot(mu(pareto_membership),chi_r(pareto_membership),'LineStyle','none','Marker','.','Color','r','MarkerSize',8);
plot(mu(pareto_membership), chi_u(pareto_membership),'LineStyle','none','Marker','.','Color','b','MarkerSize',8);
legend('\chi_c', '\chi_{er}','\chi_r','\chi_u');
xlabel('\mu (h^{-1})','FontSize',14);
ylabel('Allocation parameters','FontSize',14);


v_d= k_a * as;

figure; hold on;
plot(mu(pareto_membership), v_r(pareto_membership),'LineStyle','none','Marker','.','Color','r','MarkerSize',8);
plot(mu(pareto_membership), v_mu(pareto_membership),'LineStyle','none','Marker','.','Color','b','MarkerSize',8);
plot(mu(pareto_membership), v_mc(pareto_membership),'LineStyle','none','Marker','.','Color',[1,0.84,0],'MarkerSize',8);
plot(mu(pareto_membership), v_mer(pareto_membership),'LineStyle','none','Marker','.','Color','g','MarkerSize',8);
plot(mu(pareto_membership), v_d(pareto_membership),'LineStyle','none','Marker','.','Color','c','MarkerSize',8);
legend('v_r','v_{mu}', 'v_{mc}', 'v_{mer}', 'v_d');
xlabel('\mu (h^{-1})','FontSize',14);
ylabel('Fluxes (Cmmol gDW^{-1} h^{-1})','FontSize',14);


%% Auxiliary functions

function co = findPoints(P, m, d)
% Find coordinates co of pairs of predicted rate-yield pairs (P) within a Euclidian distance
% d of measurements m in normalized unit space
    co = [];
    for k = 1:length(P(:,1))
%        dist = max(abs((m(1)-P(k,1))/m(1)), abs((m(2)-P(k,2))/m(2)));
%        dist = sqrt(((m(1)-P(k,1))/m(1))^2 + ((m(2)-P(k,2))/m(2))^2);
        dist = sqrt(sum(((m(1,:)-P(k,:))./m(1,:)).^2));
        if dist < d
            co = [co, k];
        end
    end
end

function co = findClosestPoint(P, m)
% Find coordinate co of predicted points (P) that is closest to measured point (m) in normalized unit space
    co = 1;
    min = inf;
    for k = 1:length(P(:,1))
%         dist = max(abs((m(1)-P(k,1))/m(1)), abs((m(2)-P(k,2))/m(2)));
%        dist = sqrt(((m(1)-P(k,1))/m(1))^2 + ((m(2)-P(k,2))/m(2))^2);
        dist = sqrt(sum(((m(1,:)-P(k,:))./m(1,:)).^2));
        if dist < min
            co = k;
            min = dist;
        end
    end
end

function [co, best] = findPoints_strict(P, m, d)
% Find coordinates co  of predicted  pairs (P) within a
% square of size d of measurements m in normalized unit space
%comparasion Yield, rate and chi values

    co = [];
    dist=1000*ones(1,length(P(1,:)));

    for k = 1:length(P(:,1))
        for i=1:length(P(1,:))        
         dist(i) = abs((m(i)-P(k,i))/m(i));       
        end
         if k==1
             best_score=100; 
             best=1;
         end

% %strict selection, all criteria <d
        if dist < d
            co = [co, k];

            if sum(dist) <best_score             
                 best=k;
                 best_score= sum(dist);
            end
        end   


    end % loop on k   
end