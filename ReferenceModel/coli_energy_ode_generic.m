%% ODE model of resource allocation with coupled carbon and energy balances
% "Resource allocation accounts for the large variability of rate-yield phenotypes across bacterial strains" 
% coli_energy_ode_generic_simplifiedModel.m
% Authors: Valentina Baldazzi and Hidde de Jong, March 2022

function out = coli_energy_ode_generic_simplifiedModel(t,x,h,p)

    global beta gamma n_r n_mu n_mer n_mef;
    global corr_ru corr_mef;
    global k_a scenario; 

    % concentrations
    r = x(1);
    m_er = x(2);
    m_ef = x(3);
    m_c = x(4);
    m_u = x(5);
    c = x(6);
    as = x(7);

    % allocation parameters
    chi_u = h(1);
    chi_r = h(2);
    chi_c = h(3); 
    chi_er = h(4); 

    % fluxes and growth rate
   if  strcmp(scenario, 'glc_varying_parameters') 
    v = calculFlux_random_param(x,p,1);
   else
    v = calculFlux_generic_simplifiedModel(x,1);
   end
    
    v_mc = v(1);
    v_mer = v(2);
    v_mef = v(3);
    v_r = v(4);
    v_mu = v(5);

    mu = beta * (v_mc - v_mer - corr_mef * v_mef - (corr_ru-1) * v_mu - (corr_ru-1) * v_r) - gamma;  %mu

    %protein synthesis
    rdot = chi_r * v_r - (mu+gamma) * r;
    merdot = chi_er * v_r -(mu+gamma) * m_er; 
    mefdot = (1 - chi_u - chi_r - chi_c - chi_er) * v_r -(mu+gamma) * m_ef; 
    mcdot = chi_c * v_r - (mu+gamma) * m_c; 
    mudot = chi_u * v_r - (mu+gamma) * m_u;

    %metabolism
    cdot = v_mc - v_mer - corr_mef * v_mef - corr_ru * v_mu - corr_ru * v_r - (mu+gamma) * c;
    asdot = n_mer * v_mer + n_mef * v_mef - n_r * v_r - n_mu * v_mu - k_a * as; 

    out = [rdot; merdot; mefdot; mcdot; mudot; cdot; asdot];
    
end