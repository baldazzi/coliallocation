%% Computation of fluxes in ODE model with coupled carbon and energy balances
% "Resource allocation accounts for the large variability of rate-yield phenotypes across bacterial strains" 
% calculFlux_generic_simplifiedModel.m
% Authors: Valentina Baldazzi and Hidde de Jong, March 2022

function v = calculFlux_generic_simplifiedModel(x,n)

    global e_s; 
    global k_mu k_r k_mer k_mef;
    global K_mer K_mef K_mu K_r K_ar K_amu K_amer K_amef;
    global a0;

    v = zeros(n,5);

    % Computation of fluxes

    r = x(1,:);
    m_er = x(2,:);
    m_ef = x(3,:);
    m_c = x(4,:);
    m_u = x(5,:);
    %q= x(6,:);

    c = x(7,:);
    as = x(8,:);
    
    v(:,1) = m_c * e_s; %v_mc
    v(:,2) = m_er .* k_mer .* (c ./ (c + K_mer)) .* ((a0 - as) ./ (a0 - as + K_amer)); %v_mer
    v(:,3) = m_ef .* k_mef .* (c ./ (c + K_mef)) .* ((a0 - as) ./ (a0 - as + K_amef)); %v_mef
    v(:,4) = k_r * r .* (c ./ (K_r + c)) .* (as ./ (as + K_ar));%v_r
    v(:,5) = k_mu * m_u .* (c ./ (K_mu + c)) .* (as ./ (as + K_amu));%v_mu

end
