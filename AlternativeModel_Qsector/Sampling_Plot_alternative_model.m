%% Code for the sampling of the space of resource allocation parameters
% "Resource allocation accounts for the large variability of rate-yield phenotypes across bacterial strains" 
% Sampling_generic_paper.m
% Authors: Valentina Baldazzi and Hidde de Jong, March 2022

clear all;
close all;


%% Define and read parameters
global beta gamma n_r n_mu e_s n_mer n_mef; 
global k_mu k_r k_mer k_mef k_a;
global K_mer K_mef K_mu K_r K_ar K_amu K_amer K_amef;
global a0;
global corr_ru corr_mef;


    parameters = readtable('tableP_alternative_glc.dat');
    ratesConc = readtable('tableFRC_alternative_glc.dat');


% Define parameters
beta = ratesConc.beta;
gamma = ratesConc.g;

n_r = parameters.n_r; 
n_mu = parameters.n_mu; 

e_s = parameters.e_s;

k_mu = parameters.k_mu;
k_r = parameters.k_r;
k_a = parameters.k_a;

K_mu = parameters.K_mu;
K_r = parameters.K_r;
K_ar = parameters.K_ar;
K_amu =  parameters.K_amu;

a0 = ratesConc.a0;

corr_ru = parameters.corr_ru;
corr_mef = parameters.corr_mef;

K_amer =  parameters.K_amer;
K_mer = parameters.K_mer;
n_mer = parameters.n_mer; 
k_mer = parameters.k_mer;
k_mef = parameters.k_mef;
K_amef =  parameters.K_amef;
K_mef = parameters.K_mef;
n_mef = parameters.n_mef; 

%chi_q = ratesConc.chi_q;

%% Simulation to steady-state growth

% Simulation parameters
showSim = 0;
tstart = 0;
tfinal = 400;
tspan = [tstart:1:tfinal];
options = odeset('InitialStep', 1e-6, 'RelTol', 1e-6,'AbsTol', 1e-8);

nsamples = 50; %number of samples (random) or grid size (nsample m_u nsample)
samplingMethod = [1, 0, 0]; % [fermentation (0/1), constant q-u (0/1), sampling on grid (0/1)]

samples = generateSamples(nsamples, samplingMethod, ratesConc.chi_q );

%% Initial conditions

    steady_variables = zeros(nsamples,9+5);
    
 x0 = [6, 1 ,0.5, 1, 5, 15, 0.2, 0.007];  % r, m_er, m_ef, m_c, m_u, q, c, a* in Cmmol/gDW


 param0 = [e_s k_mu k_r k_mer k_mef K_mu K_r K_amu K_ar K_mer K_mef K_amer K_amef];
 param_names = ["e_s", "k_mu", "k_r", "k_mer", "k_mef", "K_mu", "K_r", "K_amu","K_ar", "K_mer", "K_mef", "K_amer", "K_amef"];
 param = param0;


for i=1:length(samples(:,1))
  
    % Show progress
    if mod(i,10)==0 
        disp(i); 
    end  
 

     [tout,yout] = ode23s(@(t,y) coli_energy_ode_generic(t,y, samples(i,:), param),[tstart tfinal],x0, options);


   % Store values of variables at steady state
   steady_variables(i,1) = yout(end,1); %r
   steady_variables(i,2) = yout(end,2); %m_er
   steady_variables(i,3) = yout(end,3); %m_ef
   steady_variables(i,4) = yout(end,4); %m_c
   steady_variables(i,5) = yout(end,5); %m_u
   steady_variables(i,6) = yout(end,6); %q
   steady_variables(i,7) = yout(end,7); %c
   steady_variables(i,8) = yout(end,8); %a*
   steady_variables(i,9) = 1/beta - yout(end,1) - yout(end,2) - yout(end,3) - yout(end,4) - yout(end,5) - yout(end,6)- yout(end,7); %u = 1/beta - r - m_er - m_ef - m_c - q - c
   
   steady_variables(i,10) = samples(i,1); %chi_u
   steady_variables(i,11) = samples(i,2); %chi_r
   steady_variables(i,12) = samples(i,3); %chi_c; 
   steady_variables(i,13) = samples(i,4); %chi_er; 
   steady_variables(i,14) = samples(i,5); %chi_q; 


   if showSim % Show simulation results for iteration
        figure;
        plot(tout,yout);
        disp([samples(i,1), samples(i,2), samples(i,3), samples(i,4)]); 
        waitforbuttonpress;
        close(gcf);
   end



    steady_flux = calculFlux_generic_alternativeModel(steady_variables(:,1:9)',size(steady_variables,1));


end


%% Save data

     save('Sampling_Glc_test_alternativeModel.mat', 'steady_variables','steady_flux');

%% Load data
load('Sampling_Glc_alternativeModel_Qvariable.mat');
%load('Sampling_Glc_alternativeModel_QUconstant.mat');

     
     %% Compute fluxes, concentrations, and resource allocation parameters

v_mc = steady_flux(:,1);
v_mer = steady_flux(:,2);
v_mef = steady_flux(:,3);
v_r = steady_flux(:,4);
v_mu = steady_flux(:,5);

r = steady_variables(:,1);
m_er = steady_variables(:,2);
m_ef = steady_variables(:,3);
m_c = steady_variables(:,4);
m_u = steady_variables(:,5);
q = steady_variables(:,6);
c = steady_variables(:,7);
as = steady_variables(:,8);
u = steady_variables(:,9);

chi_u = steady_variables(:,10);
chi_r = steady_variables(:,11);
chi_c = steady_variables(:,12);  
chi_er = steady_variables(:,13);
chi_q = steady_variables(:,14);

chi_ef = 1 - chi_q - chi_r - chi_u - chi_c - chi_er;

% compute rate and yield
mu = beta * (v_mc - v_mer - corr_mef*v_mef - (corr_ru-1)*v_mu - (corr_ru-1)*v_r) - gamma;  %mu
Y = (v_mc - v_mer - corr_mef*v_mef - (corr_ru-1)*v_mu - (corr_ru-1)*v_r - gamma/beta) ./ v_mc; %Y


%% Plot rate-yield plane

figure; hold on;
plot(mu,Y,'LineStyle','none','Marker','o','MarkerSize',3); %
xlim([0,1.3]);
ylim([0,1]);
xlabel('Growth rate (h^{-1})');
ylabel('Growth yield');
hold on;

%% Comparison with reference model: overlapping rate-yield plots

figure;
xlim([0,1.3]);
ylim([0,1]);
hold on;
xlabel('Growth rate (h^{-1})');
ylabel('Growth yield');


%Load results for reference model
load('../ReferenceModel/Sampling_Glc_Km12_variableMu.mat'); %variable M_u sector
%load('../ReferenceModel/Sampling_Glc_Km12.mat'); %constant M_u sector

% Retrieve fluxes for reference model
v_mc1 = steady_flux(:,1);
v_mer1 = steady_flux(:,2);
v_mef1 = steady_flux(:,3);
v_r1 = steady_flux(:,4);
v_mu1 = steady_flux(:,5);

mu1 = beta * (v_mc1 - v_mer1 - corr_mef*v_mef1 - (corr_ru-1)*v_mu1 - (corr_ru-1)*v_r1) - gamma;  %mu
Y1 = (v_mc1 - v_mer1 - corr_mef*v_mef1 - (corr_ru-1)*v_mu1 - (corr_ru-1)*v_r1 - gamma/beta) ./ v_mc1; %Y

plot(mu,Y,'LineStyle','none','Marker','.','MarkerSize',10, 'Color',[0.8,0.8,0.8]);
plot(mu1,Y1,'LineStyle','none','Marker','.','MarkerSize',10, 'Color','b');
%plot(mu,Y,'LineStyle','none','Marker','.','MarkerSize',10, 'Color',[0.8,0.8,0.8]);
%legend('Reference model', 'Model variant with Q')
legend('Model variant with Q','Reference model')

%%
   data = xlsread('../Tradeoff_data_glc_paper.xlsx', 'Batch');

         % Conversion from OD600 to gDW 
         data(96:109,4) = data(96:109,4) / 0.49;  %Glc uptake flux
         data(96:109,5) = data(96:109,5) / 0.49;  %acetate overflow flux

         %conversion from Cmmol to mmol
         data(229:232,4) = data(229:232,4) / 6;
         data(229:232,5) = data(229:232,5) / 6;



    figure; hold on;
    plot(mu,Y,'LineStyle','none','Marker','o','MarkerSize',3, 'Color',[0.8,0.8,0.8]);
  
        WTIndices = [99, 111:119, 121, 126,  128:138, 148:150, 195:198, 206:207,211:222,230 ]-1;      
        scatter(data(WTIndices,1), data(WTIndices,2),'ob','fill'); %other WT strains
        mutIndices = [3:98,100:110,120, 122:125, 127, 139:147, 199:205, 208:210, 223:229, 231:233]-1;   
        scatter(data(mutIndices,1), data(mutIndices,2),'or','fill'); %mutant strains
        ALEIndices = [151:194]-1;
        scatter(data(ALEIndices,1), data(ALEIndices,2),'o','MarkerFaceColor',[0.0,0.65,0],'MarkerEdgeColor','none'); %ALE strains
        scatter(data(1,1), data(1,2),'ok','fill'); %WT calibration
        legend('Alternative model','Wild-type','Mutant','ALE','Calibration','Location', 'SouthEast');
        title('Glc');
       
 
    xlim([0,1.4]);
    ylim([0,1]);
    xlabel('Growth rate (h^{-1})');
    ylabel('Growth yield');






%% Auxiliary functions

function s = generateSamples(ns, sm, p_x)

    % ns = number of samples
    % sm = sampling method
    % sc = scenario
    % p_q = chi_q value from calibration
    % s = matrix of samples (dimensions: nsx4)

    fermentation = sm(1);
    qxConstant = sm(2);
   % gridSampling = sm(3);

 

          rng shuffle

        % Set resource allocation schemes
        for i = 1:ns
           
             if qxConstant
                  chi_q = p_x;
                  chi_u = 0.18;     
             else
                  chi_q = 0.50 + 0.04*rand;
                  chi_u = 0.14 + (1 - chi_q - 0.14)*rand; %minimum chi_u value               
            end

            chi_r = (1 -  - chi_u) * rand;
            chi_c = (1 - chi_q - chi_u - chi_r) * rand;
   
            if fermentation
                chi_er = (1 - chi_q - chi_u - chi_r - chi_c) * rand;
            else
                chi_er = (1 - chi_q - chi_u - chi_r - chi_c);             
            end

            s(i,:) = [chi_u, chi_r, chi_c, chi_er, chi_q]; % save allocation scheme
        end   
    end


