# Description :

Supporting code for "Resource allocation accounts for the large variability of rate-yield phenotypes across bacterial strains" 

Authors: Valentina Baldazzi and Hidde de Jong, January 2023

https://elifesciences.org/articles/79815


# Contents :

- Excel files : Experimental data on 
	- rate-yield  values for  different E.coli strains grown on glucose and glycerol cultures 
	- acetate overflow for different growth rates

- Matlab .dat files :

	- Model parameters and calibration fluxes for different  culture conditions  (glucose , glycerol, batch and/or chemostat) of BW25113 strain and for MG1655 strain (glucose batch culture)

- Matlab .mat files :

    - Simulation results for different culture conditions (glucose, glycerol, batch and/or chemostat), strains and model definition

- Matlab . m script files 


# Usage:

**Model calibration**

Run Matlab "*calibrationProcedure_*"  script for the selected culture condition :

- glc = Glucose batch culture
- gly = Glycerol batch culture
- glc02 = Glucose chemostat culture at dilution D=0,2 h-1
- glc035 = Glucose chemostat culture at dilution D=0,35 h-1
- glc05 = Glucose chemostat culture at dilution D=0,5 h-1
- glc_MG = Glucose batch culture for MG1655

This yield two .dat files :

*tableP_simplified_*:  containing the kinetic parameters of the model

*tableFRC_simplified_* : collecting fluxes, rates, concentrations, and resource allocation parameters for the selected condition


**Sampling of possible resource allocation scheme** 

Open "*Sampling_generic_paper.m*"  script

Select the desired simulation condition by means of the scenario variable 

6 options are available:

- glc = Glucose batch culture (fixed parameters)
- gly = Glycerol batch culture (fixed parameters)
- glc02 = Glucose chemostat culture at dilution D=0,2 h-1
- glc035 = Glucose chemostat culture at dilution D=0,35 h-1
- glc05 = Glucose chemostat culture at dilution D=0,5 h-1
- glc_varying_parameters = Glucose batch culture with varying catalytic constants k_mc, k_mef, k_mer

Select the sampling method (line 91):

- model with fermentation (0/1)
- constant Mu fraction (0/1). A 10% variation is applied otherwise, according to data by Schmidt et . (2016)
- sampling on a grid at fixed alpha_e value (0/1). Random sampling otherwise

Run the script. Simulation results are saved in a .mat file


**Plot simulation results**

Open "*Plot_results_revision.m*" script


Select the desired simulation condition by means of the scenario variable

8 options are available:

- glc= plots for glucose batch culture, with constant parameters
- glc_3D = 3D plots for glucose batch culture (Fig S6)
- glc_grid = countour plot for glucose batch culture (Fig S6)
- glc_varying_parameters = plots for glucose batch culture with varying catalytic constants
- gly= plots for glycerol batch culture
- chemostat = plot for chemostat culture, at D=0.2,0.35 and 0.5 h-1
- glc_MG = plot for MG1655 simulations under glc batch culture
- glc_NCM= Comparison between BW25113 and NCM3722 strains. Model prediction for NCN 		phenotypes (Fig 5 and Fig S5)
	
	
Run the script. 



